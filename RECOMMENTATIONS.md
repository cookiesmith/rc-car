# Recomendações gerais para o projeto

* Antes de usar o makefile, adicione-o ao seu .gitignore, e altere o
* caminho

```make
BINARIES = /home/home/usr/share/msp430-gcc

```

* Nunca altere pinos que não precisa alterar:

```C
	FOO |= BIT1; //Ativando só o pino 1, pois preciso
	BAR = BIT1; //A vida tá facil. Deixa eu fazer besteira
```

* Crie macros, em vez de const

Ambos farão a mesma coisa, mas a const gasta memória com uma coisa que
nunca vais alterar. "define" usa pré processamento para substituir a strng
da macro que criaste pelo valor da macro.

```C

#define ON BIT1+BIT2+BIT3

	const int on = BIT1+BIT2+BIT3;
	PORT1CTR = on;
	PORT1CTR = ON;

	
```

* Crie flags de debug e use um debugger, PELO AMOR DE DEUS

já deu de printf, né

```C
#define NOMEM 1

	int * a = malloc(sizeof(int));
	if (! &a) return NOMEM;
```

