﻿#include <msp430.h> 
#include <stdio.h>

/**
 * main.c
 * Author: Luiz Henrique Avilla Vigiato
 */

void config_ini(void);
void ini_P1_P2(void);
void ini_Timer0(void);
void ini_USCI_A0_UART(void);
void ini_adc10(void);
void ini_Timer1(void);


unsigned char TX_DATA[32];
unsigned char RX_DATA[32],i;
unsigned char tx_index = 0, rx_index = 0;
unsigned int x[16],y[16],ysum = 0,xsum = 0;
unsigned char bool = 0; // 0 -> Porta 4 && 1 -> Porta 5
unsigned char status = 1; // 0 -> Desligado && 1 -> Ligado

unsigned int valx = 0;
unsigned int valy = 0;
unsigned int recep = 0;

void main(void)
{
    config_ini();
    ini_P1_P2();
    ini_Timer0();
    ini_USCI_A0_UART();
    ini_adc10();
    ini_Timer1();
    while(1){

    }

}

void config_ini(void){
   /**
    * Para o contador do watchdog timer
    * Configuracoes do BCS
    * MCLK = DCOCLK ~ 1 MHz
    * ACLK = LFXT1CLK = 32768 Hz
    * SMCLK = DCOCLK / 8 ~ 125 kHz
    *
    */

    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer
    DCOCTL = CALDCO_8MHZ; // Freq. Calibrada de 8 MHz
    BCSCTL1 = CALBC1_8MHZ;
    BCSCTL3 = XCAP0 + XCAP1; // Capacitor do cristal ~12.5 pF
    while (BCSCTL3 & LFXT1OF);
    __enable_interrupt(); // seta o bit GIE - permite geracao de interrupcoes
}

void ini_USCI_A0_UART(void){
    /**
     * Taxa: 9600 bps
     * Dados: 8 bits
     * Stop bits: 1
     * paridade: sem paridade
     *
     * clock: SMCLK ~ 8MHz
     * sobreamostragem: nao utilizada
     *
     *
     */
    UCA0CTL1 |= UCSWRST;
    UCA0CTL0 = 0;
    UCA0CTL1 = UCSSEL1 + UCSWRST; // Fonte clock: SMCLK ~ 8 MHz
    UCA0BR0 = 0x41; // Para 9600 bps, conforme Tabela 15-4 do User Guide
    UCA0BR1 = 0x03;
    UCA0MCTL = UCBRS1;

    P1SEL |= BIT1 + BIT2;//configuracao de porta 1
    P1SEL2 |= BIT1 + BIT2;

    UCA0CTL1 &= ~UCSWRST; // Coloca a interface no modo normal
    IFG2 &= ~UCA0TXIFG; // Limpa TXIFG, pois o bit é setado após reset
    IE2 |= UCA0TXIE + UCA0RXIE; // Habilitando a geração de int. para RX e TX
}

void ini_adc10(void){
    /**
     *  Configuracao adc10 - medicao temperatura interna
     *
     *  - Clock: ADC10OSC ~ 6MHz
     *  - Entrada de sinal: A5 E A4
     *  - Taxa de conversao maxima: 200 ksps
     *  - Tensao de referencia: VR+ = Vcc; VR- = 0V;
     *  - Gatilho SOFTWARE
     */


    //LEMBRAR QUE O ADC10ON E DEPENDENTE DO BOTAO
    ADC10CTL0 = ADC10SHT0 + MSC + ADC10ON + ADC10IE;
    ADC10CTL1 = INCH_5 + CONSEQ_0; //Canal 5 - single conversion
    ADC10AE0 = BIT5 + BIT4; // Habilita canal A5

    ADC10CTL0 |= ENC; // Habilita convesao (gera borda de subida)
}

void ini_P1_P2(void){
    /**
     * P1.x - N.C. -> Todas sao saidas
     * P1.6 - Led 2 -> Nivel alto
     */
    P1DIR = 0xFF;
    P1OUT = BIT6; // P1.6 -> Saida em nivel alto (Led 2) || P1.x -> Saida em nivel baixo




    /**
     * P2.0 -> entrada botao -> Entrada pull-up - Resistor Habilitado - Borda de descida
     * P2.x -> Saida em nivel baixo
     */
    P2DIR = BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6 + BIT7;
    P2OUT = BIT0;
    P2REN = BIT0;
    P2IFG = 0;
    P2IE = BIT0;
    P2IES = BIT0;
}

void ini_Timer0(void){
    /* CONFIG. TIMER 0 A para gerar GATILHO DE CONVERSAO por SW
     * >>> CONTADOR
     * - Clock: ACLK 32.768 Hz .:. - Modo contador: Up .:.
     * - Int. contador: Habilitada
     *
     */
     TA0CTL = TASSEL0 + MC0;
     TA0CCTL0 = CCIE;
     TA0CCR0 = 2048;
}

void ini_Timer1(void){
    /**
     * Timer 1 para deboucer de Botao
     * Contador:
     *  - clock = SMCLK ~ 1MHz
     *  - modo = up -> inicialmente parado
     *  - interrupcao = desabilitada
     *
     * modulo 0:
     *  - funcao: comparacao (nativo)
     *  - TA1CCR0 = 0 -> contador parado
     *  - TA1CCR0 (25 ms) = 25000
     *  - Interrupcao: Habilitada
     *
     */

    TA1CTL = TASSEL1 + ID0 + ID1;
    TA1CCTL0 = CCIE;
    TA1CCR0 = 50000;
}

#pragma vector = PORT2_VECTOR
__interrupt void RTI_da_P2_Encoder(void){
    /**
     * Desabilita interrupcao da porta
     * Inicia contagem pra debounce
     * verifica estado e alterna
     */
    TA1CTL |= MC0;
    P2IE = 0;
    P2IFG = 0;
}

#pragma vector=TIMER1_A0_VECTOR
__interrupt void TimerA1_RTI(void){
    /**
     * Desabilitar timer
     * limpa flag
     * habilita inter. novamente
     */

    if(P2IN == BIT0){

    } else {
        TA1CTL &= ~MC0;
        P2IE = BIT0;
        P2IFG = 0;
        if(status){
            /*
             * Desabilita cronometro e reinicia
             * Desabilita ADC10
             * Muda status para 0
             * Passa o micro para estado economico
             * Desliga led 2
             */
            TA0CTL &= ~MC0;
            ADC10CTL0 &= ~ENC;
            P1OUT &= ~BIT6;
            sprintf(&TX_DATA[0], "disable\n");
            UCA0TXBUF = TX_DATA[0];
            tx_index++;
            status = 0;
        } else {
            /**
             * Reabilita cronometro e reinicia ele
             * Habilita ADC10
             * Muda status para 1
             * Passa o micro para estado comum
             */
            TA0CTL |= MC0;
            ADC10CTL0 |= ENC;
            P1OUT |= BIT6;
            sprintf(&TX_DATA[0], "enable\n");
            UCA0TXBUF = TX_DATA[0];
            tx_index++;
            status = 1;
        }
    }

}


#pragma vector = TIMER0_A0_VECTOR
__interrupt void RTI_do_M0_do_Timer0(void){
    /**
     * Disparar a conversao A/D -> Borda de subida
     */
    ADC10CTL0 |= ADC10SC + ENC;

}

#pragma vector=USCIAB0TX_VECTOR
__interrupt void RTI_TXD(void){
    IFG2 &= ~UCA0TXIFG;
    if( TX_DATA[tx_index] == '\0' ){
        tx_index = 0;
    }else{
        if(tx_index >= 32){
            tx_index = 0;
        }else{
            UCA0TXBUF = TX_DATA[tx_index];
            tx_index++;
        }
    }
}


#pragma vector=USCIAB0RX_VECTOR
__interrupt void RTI_RXD(void){
    if(RX_DATA[rx_index] == 13 || RX_DATA[rx_index] == 10){
        rx_index = 0;
        if(valx < 1024){
            //Se valx > 1024 nao fazer nada -- Nao mexer no pwm
        }
        if(valy < 1024){
            //Se valy -- Nao mexer no pwm
        }
        valx = 0;
        valy = 0;
    }
    RX_DATA[rx_index] = UCA0RXBUF;
    if(recep == 1){
        if(RX_DATA[rx_index] != 45){

            if(valx != 0){
                valx = valx * 10;
                valx = valx + (RX_DATA[rx_index] - 48);
            } else {
                valx = RX_DATA[rx_index] - 48;
            }
        } else {
            recep = 0;
        }
    }
    if(recep == 2){
        if(RX_DATA[rx_index] != 13){

            if(valy != 0){
                valy = valy * 10;
                valy = valy + (RX_DATA[rx_index] - 48);
            } else {
                valy = RX_DATA[rx_index] - 48;
            }
        } else {
            recep = 0;
        }

    }

    if(RX_DATA[rx_index] == 88){ // ASCII X = 88
        recep = 1;
    }
    if(RX_DATA[rx_index] == 89){ // ASCII Y = 89
        recep = 2;
    }
    if(RX_DATA[rx_index] == 100){ // disable

    }
    if(RX_DATA[rx_index] == 101){ // enable

    }

    rx_index++;
}

#pragma vector = ADC10_VECTOR
__interrupt void RTI_do_ADC10(void){

    ADC10CTL0 &= ~ENC;

    if(bool){
        //Leitura sinal X
        xsum = 0;
        for(i=0;i<15;i++){
            x[i] = x[i+1];
            xsum = xsum + x[i];
        }
        x[15] = ADC10MEM;
        xsum = xsum + x[15];
        xsum = (int)(1024-(xsum/16));
        /**
         * Troca o ADC para saida A4
        */
        ADC10CTL1 = INCH_4 + CONSEQ_0;// Canal 4 - single conversion
        ADC10AE0 = BIT5 + BIT4; // Linha desnecessaria?
        bool = 0; // Proximo estado
        ADC10CTL0 |= ADC10SC + ENC; // Habilita conversao para proxima entrada

    } else {
        //Leitura sinal Y
        ysum = 0;
        for(i=0;i<15;i++){
            y[i] = (int)(y[i+1]);
            ysum = ysum + y[i];
        }
        y[15] = (int)(ADC10MEM);
        ysum = ysum + y[15];
        ysum = (int)(ysum/16);

        /**
          * Troca o ADC para saida A5
         */
        ADC10CTL1 = INCH_5 + CONSEQ_0;// Canal 5 - single conversion
        ADC10AE0 = BIT5 + BIT4;
        bool = 1;// Devolve estado inicial

        //Passa para a UART os valores lidos
        sprintf(&TX_DATA[0], "X%d-Y%d\n",xsum,ysum);
        UCA0TXBUF = TX_DATA[0];
        tx_index++;

        ADC10CTL0 |= ENC;
    }
}
