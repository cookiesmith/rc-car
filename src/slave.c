#include <msp430.h> 

//Left H-brigde configuration for forward moviment
#define HLFWD P1OUT |= BIT3+BIT5; P1OUT &= ~BIT4; P2OUT &= ~BIT0;
//Left H-brigde configuration for reverse moviment
#define HLREV P1OUT &= ~(BIT3+BIT5); P1OUT |= BIT4; P2OUT |= BIT0;
//Right H-brigde configuration for forward moviment
#define HRFWD P1OUT |= BIT6; P1OUT &= ~BIT7; P2OUT |= BIT6; P2OUT &= ~BIT7;
//Right H-brigde configuration for reverse moviment
#define HRREV P1OUT &= ~BIT6; P1OUT |= BIT7; P2OUT &= ~BIT6; P2OUT |= BIT7;

unsigned char RX_data [32];
unsigned char rx_index = 0;
int x, y, i;
int recep, count = 1, disable = 0;


void init_clock(void)
{
	WDTCTL = WDTPW | WDTHOLD;
	DCOCTL = CALDCO_8MHZ;
	BCSCTL1 = CALBC1_8MHZ;

	__enable_interrupt();
}

void init_ports(void)
{
	P1DIR = P2DIR = 0xFF;
	P1OUT = P2OUT = 0;

	P1SEL |= BIT1 + BIT2; // Set P1SEL.X (3 and 4) as RX and TX for UART
	P1SEL2 |= BIT1 + BIT2; // Set P1SEL.X (3 and 4) as RX and TX for UART
	
	P2SEL |= BIT1 + BIT2 + BIT4 + BIT5; // Set P2SEL.X (1, 2, 4 and 5) to PWM/timer output pins
	P2SEL &= ~(BIT6 + BIT7); //Set function to I/O
	P2SEL2 &= ~(BIT6 + BIT7); //Set function to I/O
}

void engine(int x, int y)
{//Configure the needed polarity, and configure the pwm
	int pwm_ref;

	if (x > 548) //The car will go forward
	{
		HLFWD //Configure the polarity for such
		HRFWD
		pwm_ref = (x-548)/476.0;
		if(y > 548) //The car is going right, refence on left, and subtract porcentage from right
		{
			TA1CCR1 = 800*pwm_ref;
			TA1CCR2 = pwm_ref*(1-(y-548)/476);
		}
		else if(y < 480) //The car is going right, so invert
		{
			TA1CCR2 = 800*pwm_ref;
			TA1CCR1 = pwm_ref*(y/476);
		}
		else //Dead zone for Y: The car is going straight
		{
			TA1CCR1 = TA1CCR2 = 800*pwm_ref;
		}
	}
	if (x < 476) //The car is going reverse: the same commands
	{
		HLREV
		HRREV
		pwm_ref = (1-x/476);
		if(y > 548)
		{
			TA1CCR1 = 800*pwm_ref;
			TA1CCR2 = pwm_ref*(1-(y-548)/476);
		}
		else if(y < 476)
		{
			TA1CCR2 = 800*pwm_ref;
			TA1CCR1 = pwm_ref*(y/476);
		}
		else
		{
			TA1CCR1 = TA1CCR2 = 800*pwm_ref;
		}
	}
	else //Dead zone X:forward/reverse moviment. only rotation is aplied
	{
		if(y > 548)
		{
			HLFWD
			HRREV
			TA1CCR1 = TA1CCR2 = 800*(y-548)/476;
		}
		else if(y < 476)
		{
			HLREV
			HRFWD
			TA1CCR2 = TA1CCR1 = 800*(1-y/476);
		}
		else //Dead zone for rotation, the car stops
		{
			TA1CCR1 = TA1CCR2 = 0;
		}
	}
}

void init_pwm(void)
{
	/* PWM signal configuration
	*
	*	- fPWM 10kHz
	*
	*	- fCLK = SMCLK = 8 MHz, UP mode
	*
	*	- TA0CCR0 = (8M / 10k) - 1 =  799
	*
	*	- Reset/Set mode (7)
	*
	*/

	TA1CTL = TASSEL1 + MC0; //SMCLK in UP mode
	TA1CCTL1 = TA1CCTL2 = OUTMOD0 + OUTMOD1 + OUTMOD2 + OUT; //reset/set count
	TA1CCR1 = TA1CCR2 = 0; //Initial Duty Cicle as 0
	TA1CCR0 = 799; //Set fPWM to 10k

	//The PWM output pins are set in init_ports
}

void init_uart(void)
{

	UCA0CTL1 |= UCSWRST; //Put UART in RESET for initialization
	UCA0CTL0 = 0; //No parity, LSB, 8-bit data, one stop bit, UART
	UCA0CTL1 = UCSSEL1 + UCSWRST; //Select SMCLK as UART clock

	// According to the table, must be 833
	UCA0BR0 = 0x41; //65
	UCA0BR1 = 0x03; //768

	UCA0MCTL = UCBRS1;

	UCA0CTL1 &= ~UCSWRST; // Release for operation
	IFG2 &= ~UCA0TXIFG; //Not needed
	IE2 |= UCA0RXIE;

	//The output pins are configured in init_port()
}

__attribute__ ((interrupt(USCIAB0RX_VECTOR)))
void interrupt_uart_rx(void)
{
	if(RX_data[rx_index - 1] == '\x0a') //ASCII new line
	{
		x = y = 0;
		for(int i = 0; i <= rx_index-1; i++)
		{
			if(RX_data[i-1] == 88 && RX_data[i] < 69) 
			{//X
				recep = 1;
			}
			if(recep == 1) //The number are composed by (a1)*10^n+(a2)*10^n-1+...+(an)*10^0
			{//We are recieving a number
				if(x == 0)
				{//I'ts the first number
					x = RX_data[i] - 48;
				}
				else
				{//It's a 10^n number
					if(RX_data[i] != 45)
					{
						x = x*10 + RX_data[i] - 48;
					}
					else
					{//We've read the entire number
						recep = 0;
					}
				}
			}
			if(RX_data[i-1] == 89 && RX_data[i] < 69)
			{//Y
				recep = 2;
			}
			if(recep == 2){
				if(y == 0)
				{
					y = RX_data[i] - 48;
				}
				else
				{
					if(RX_data[i] != 10)
					{
						y = y*10 + RX_data[i] - 48;
					}
					else
					{
						recep = 0;
					}
				}
			}
		}
		if (disable)
		{//Put the engines in off mode
			engine(512, 512);
		}
		else
		{//Pass a common speed
			engine(x, y);
		}

		rx_index = 0;
	}
	RX_data[rx_index] = UCA0RXBUF;
	if(RX_data[rx_index] == 100)
	{ // disable
		disable = 1;
		TA1CCR0 = 0;
	} else
	if(RX_data[rx_index] == 101)
	{ // enable
		disable = 0;
		TA1CCR0 = 799;
	}
	rx_index++;
}

int main(void)
{
	init_clock(); //Initializes the system clocks
	init_ports(); //Initalize ports 1 and 2
	init_pwm(); //Initialize PWM timer
	init_uart(); //Initialize UART protocol

	while(1)
	{
	}
	return 0;
}

