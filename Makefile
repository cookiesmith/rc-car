PROJECT=rc-car
DEVICE = MSP430G2553

BUILD = bin
SOURCE = src
OBJECTS = master.o slave.o
#MAP = $(BUILD)/$(PROJECT).map

BINARIES = /home/home/usr/share/msp430-gcc
GCC_DIR = $(BINARIES)/bin
SUPPORT_FILE_DIRECTORY = $(BINARIES)/include

CC = $(GCC_DIR)/msp430-elf-gcc
GDB = $(GCC_DIR)/msp430-elf-gdb

CFLAGS = -I $(SUPPORT_FILE_DIRECTORY) -mmcu=$(DEVICE) -Wall -ggdb
LFLAGS = -L $(SUPPORT_FILE_DIRECTORY) -Wl,--gc-sections 
#LFLAGS = -L $(SUPPORT_FILE_DIRECTORY) -Wl,-Map,$(MAP),--gc-sections 

all: ${OBJECTS}
	$(CC) $(CFLAGS) $(LFLAGS) $(BUILD)/master.o -o $(BUILD)/master.elf
	$(CC) $(CFLAGS) $(LFLAGS) $(BUILD)/slave.o -o $(BUILD)/slave.elf
clean:
	$(RM) $(BUILD)/*
debug: all

master.o:
	$(CC) $(CFLAGS) $(LFLAGS) -c $(SOURCE)/master.c -o $(BUILD)/master.o
slave.o:
	$(CC) $(CFLAGS) $(LFLAGS) -c $(SOURCE)/slave.c -o $(BUILD)/slave.o
