# rc-car

Trabalho de Sistemas Microcontrolados (2022-1)

Controle de CHASIS 4WD usando um shield JOYSTICK

## [RECOMENDAÇÕES GERAIS](RECOMMENTATIONS.md)

## OBJETIVO

Desenvolver um sistema para controle de um veículo com 4 rodas formado por
um shield do tipo joystick. Este trabalho está dividido nas seguintes
etapas:

* (2,5) JOYSTICK microcontrolado:

	* (1,0) Leitura dos sinais analógicos do shield joystick usando o
	 ADC10.

	* (0,5) Montar protocolo para envio dos dados ao chassis usando um
	 módulo UART - Bluetooth. OBS.: um kit no chassi e outro no
	 "joystick";

	* (1,0) Push-bottom do joystick para ligar/desligar o joystick e o
	 veículo (desligar rádios, drivers de motores e colocar os uCons
	 em LPM3/LPM4);

* (3,0) Chassis 4WD:

	* (1,0) Geração de sinais PWM para controle dos motores (por
	 software), os  quais serão acionados por módulos ponte H;

	* (2,0) Algoritmo para leitura de comandos recebidos da
	 UART/Bluetooth e  controle dos motores a partir dos dados
	 provenientes dos potenciômetros, utilizando as tensões medidas
	 para controlar:

		* (1,0) Aceleração suave (média móvel), proporcional ao
		 deslocamento do joystick;

		* (1,0) Direção de percurso suave (média móvel), proporcional ao
		 deslocamento do joystick;

* (1,5) Configuração das interfaces UART do kit do chassi e do kit
 do "joystick" usando 1 par de módulos UART - bluetooth;

* (2,5) Integração/funcionamento do sistema.

Para funcionamento PARCIAL a nota deste item será 1,0. Para funcionamento
COMPLETO/TOTAL a nota será 2,5

## ATIVIDADES:

Membros 1 e 2: Inicialização de periféricos e geração de sinais PWM para
controle dos motores DC.  Pareamento e testes dos módulos Bluetooth-UART;

Membro 2, 3 e 4: Aquisição dos sinais dos potenciômetros do shield joystick
para controle dos motores DC.  Desenvolvimento de protocolo simples de
comunicação e envio dos dados pela UART-Bluetooth para o kit no chassis;

Membro 4, 5 e 6: recepção de dados via Bluetooth-UART e desenvolvimento de
algoritmo para atuar nos sinais PWM.
